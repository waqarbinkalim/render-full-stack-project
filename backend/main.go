package main

import (
	"github.com/gin-gonic/gin"
	"github.com/robfig/cron/v3"
	"net/http"
)

func main() {
	r := gin.Default()

	go startCronJob()

	r.GET("/", helloWorld)
	r.GET("/ping", ping)
	r.GET("/health", healthCheck)

	r.Run()
}

func startCronJob() {
	c := cron.New()

	c.AddFunc("@every 14m", func() {
		http.Get("http://localhost:8080/ping")
	})

	c.Start()
}

func helloWorld(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "Hello, World!",
	})
}

func ping(c *gin.Context) {
	c.JSON(http.StatusOK, "ping")
}

func healthCheck(c *gin.Context) {
	c.JSON(http.StatusOK, "OK")
}
